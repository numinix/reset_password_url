<?php
/**
 * Header code file for the  Password reset page
 *
 * @package page
 * @copyright Copyright 2003-2011 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: header_php.php 18697 2011-05-04 14:35:20Z wilt $
 */
// This should be first line of the script:

require(DIR_WS_MODULES . zen_get_module_directory('require_languages.php'));
$error = false;
$fatal_error = false;

if (!isset($_GET['reset_token']) && !isset($_POST['reset_token'])) {
  $fatal_error = true;
  $messageStack->add('reset_password', PASSWORD_RESET_ENTRY_PASSWORD_TOKEN_ERROR);
}


$reset_token = isset($_GET['reset_token']) ? zen_db_prepare_input($_GET['reset_token']) : zen_db_prepare_input($_POST['reset_token']);

$check_customer_query = "SELECT customers_nick, customers_id
                             FROM   " . TABLE_CUSTOMERS . "
                             WHERE  password_reset_token = :reset_token";

$check_customer_query = $db->bindVars($check_customer_query, ':reset_token', $reset_token, 'string');

$check_customer = $db->Execute($check_customer_query);

if(!($check_customer->fields['customers_id'] > 0)){
  $messageStack->add('reset_password', PASSWORD_RESET_ENTRY_PASSWORD_TOKEN_ERROR);
  $error = true;
  $fatal_error = true;
}
else {
  $customer_id = $check_customer->fields['customers_id'];
  $nickname = $check_customer->fields['customers_nick'];
}

if (isset($_POST['action']) && ($_POST['action'] == 'process')) {
  $password_new = zen_db_prepare_input($_POST['password_new']);
  $password_confirmation = zen_db_prepare_input($_POST['password_confirmation']);

  $error = false;

  if (strlen($password_new) < ENTRY_PASSWORD_MIN_LENGTH) {
    $error = true;
    $messageStack->add('reset_password', ENTRY_PASSWORD_NEW_ERROR);
  } elseif ($password_new != $password_confirmation) {
    $error = true;
    $messageStack->add('reset_password', ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING);
  }

  if ($error == false) {
      $nickname = $check_customer->fields['customers_nick'];
      //now that the account has a password, we mark COWOA as false so that the customer can access features like store credit.
      $sql = "UPDATE " . TABLE_CUSTOMERS . "
              SET customers_password = :password,
              password_reset_token = '',
              COWOA_account = '0' 
              WHERE customers_id = :customersID";

      $sql = $db->bindVars($sql, ':customersID',$customer_id, 'integer');
      $sql = $db->bindVars($sql, ':password',zen_encrypt_password($password_new), 'string');
      $db->Execute($sql);

      $sql = "UPDATE " . TABLE_CUSTOMERS_INFO . "
              SET    customers_info_date_account_last_modified = now()
              WHERE  customers_info_id = :customersID";

      $sql = $db->bindVars($sql, ':customersID',$_SESSION['customer_id'], 'integer');
      $db->Execute($sql);

        if ($phpBB->phpBB['installed'] == true) {
          if (zen_not_null($nickname) && $nickname != '') {
            $phpBB->phpbb_change_password($nickname, $password_new);
          }
        }
      $messageStack->add_session('login', PASSWORD_RESET_SUCCESS_PASSWORD_UPDATED, 'success');
      zen_redirect(zen_href_link(FILENAME_LOGIN, '', 'SSL'));
  }
}

//$breadcrumb->add(NAVBAR_TITLE_1, zen_href_link(FILENAME_ACCOUNT, '', 'SSL'));
//$breadcrumb->add(NAVBAR_TITLE_2);

// This should be last line of the script:
//$zco_notifier->notify('NOTIFY_HEADER_END_ACCOUNT_PASSWORD');
