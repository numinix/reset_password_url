<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: reset_password.php $
 */

define('FILENAME_RESET_PASSWORD', 'reset_password');
define('PASSWORD_RESET_HEADING_TITLE', 'Password Reset');
//define('LOGIN_PAGE', 'quick_checkout'); //page where user logs in after resetting password. Usually 'login' or 'quick_checkout'.
define('PASSWORD_RESET_ENTRY_PASSWORD_TOKEN_ERROR', 'Your password cannot be reset at this time. Please ensure that you have copied the URL exactly as it appears in the email that you received.');
define('PASSWORD_RESET_SUCCESS_PASSWORD_UPDATED', 'Your password has been successfully updated. Please login using your email address and the password you have just created.');

define('PASSWORD_RESET_PAGE_TEXT', 'Enter your email address below and we\'ll send you instructions on how to reset your password.');
define('PASSWORD_RESET_EMAIL_BODY', 'A password reset was requested from ' . $_SERVER['REMOTE_ADDR']  . '.' . "\n\n" . 'To reset your password at \'' . STORE_NAME . '\'  please visit the following URL:' . "\n\n" . 
HTTPS_SERVER . DIR_WS_CATALOG .'index.php?main_page=reset_password&reset_token=%s' . "\n\n");
define('PASSWORD_RESET_SUCCESS_PASSWORD_SENT', 'Instructions to reset your password have been sent to your email address.');
define('PASSWORD_RESET_EMAIL_SUBJECT', STORE_NAME . ' - Password Reset');


