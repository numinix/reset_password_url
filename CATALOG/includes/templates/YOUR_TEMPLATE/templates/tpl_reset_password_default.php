<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=reset_password.<br />
 * Allows customer to change their password
 *
 * @package templateSystem
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_reset_password_default.php $
 */
?>

<div class="centerColumn" id="accountPassword">
	<div class="alert forward"><?php echo FORM_REQUIRED_INFORMATION; ?></div> 
	<h1><?php echo PASSWORD_RESET_HEADING_TITLE; ?></h1>
	<?php echo zen_draw_form('account_password', zen_href_link(FILENAME_RESET_PASSWORD, '', 'SSL'), 'post', 'onsubmit="return check_form(account_password);"') . zen_draw_hidden_field('action', 'process'). zen_draw_hidden_field('reset_token', $reset_token); ?>

		<fieldset>
			
			
			
			<br class="clearBoth" />

			<?php if ($messageStack->size('reset_password') > 0) echo $messageStack->output('reset_password'); ?>

			<?php if(!$fatal_error){ ?>
			<div class="field__container">
				<div class="field">
					<label class="inputLabel" for="password-new"><?php echo ENTRY_PASSWORD_NEW; ?> <?php echo (zen_not_null(ENTRY_PASSWORD_NEW_TEXT) ? '<span class="alert">' . ENTRY_PASSWORD_NEW_TEXT . '</span>': ''); ?></label>
					<?php echo zen_draw_password_field('password_new','','id="password-new"'); ?>
				</div>

				<div class="field">
					<label class="inputLabel" for="password-confirm"><?php echo ENTRY_PASSWORD_CONFIRMATION; ?> <?php echo (zen_not_null(ENTRY_PASSWORD_CONFIRMATION_TEXT) ? '<span class="alert">' . ENTRY_PASSWORD_CONFIRMATION_TEXT . '</span>': '') ?></label>
					<?php echo zen_draw_password_field('password_confirmation','','id="password-confirm"'); ?>
				</div>
				
				<div class="button__container">
				 	<div class="buttonRow"><?php echo zen_image_submit(BUTTON_IMAGE_SUBMIT, BUTTON_SUBMIT_ALT); ?></div>
				 	<div class="buttonRow"><?php echo '<a href="' . zen_href_link(FILENAME_ACCOUNT, '', 'SSL') . '">' . BUTTON_BACK_ALT . '</a>'; ?></div>
				</div>
			</div>
			<?php } ?>
		</fieldset>
	</form>
</div>
