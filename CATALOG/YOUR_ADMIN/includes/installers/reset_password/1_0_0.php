<?php
// use $configuration_group_id where needed

 $columns = $db->metaColumns(TABLE_CUSTOMERS);
 if(!$columns['PASSWORD_RESET_TOKEN']) {
   $db->Execute("ALTER TABLE " . TABLE_CUSTOMERS . " ADD COLUMN password_reset_token VARCHAR(100);");
 }

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_group_id, sort_order, configuration_key, configuration_title, configuration_value, configuration_description, set_function) VALUES (" . (int)$configuration_group_id . ", 3, 'RESET_PASSWORD_URL_TYPE', 'Password reset method', 'Temporary Password', 'Method available to customers to reset their password. (default: Temporary Password)', 'zen_cfg_select_option(array(\'Temporary Password\', \'Password Reset URL\'),');");

// For Admin Pages

$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
if ($zc150) { // continue Zen Cart 1.5.0
  // delete configuration menu
  $db->Execute("DELETE FROM ".TABLE_ADMIN_PAGES." WHERE page_key = 'configPasswordResetURL' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('configPasswordResetURL')) {
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('configPasswordResetURL',
                              'BOX_RESET_PASSWORD_URL', 
                              'FILENAME_CONFIGURATION',
                              'gID=' . $configuration_group_id, 
                              'configuration', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Password Reset URL Configuration Menu.', 'success');
    }
  }
}
